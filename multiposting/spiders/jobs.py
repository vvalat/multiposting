import scrapy

class JobOffersSpider(scrapy.Spider):
    name = "offers"

    start_urls = [
            'http://jobs.careerpage.fr/career/multiposting-jobs-fr/',
            'http://jobs.careerpage.fr/career/multiposting-internship-fr/',
        ]

    def parse(self, response):
        for result in response.css('table.results').css('tbody').css('tr'):
            detail_page = result.css('td.view').css('a::attr(href)').extract_first()
            if detail_page is not None:
                yield scrapy.Request(response.urljoin(detail_page), callback=self.parse_details)

    def parse_details(self, response):
        def get_css(query):
            return response.css(query).extract_first()

        def get_xpath(query):
            return response.xpath(query).extract_first()

        loc_arr_len = len(response.xpath('//div[@id="advanced_search"]/li[2]/span[@class="value"]/text()').extract_first().split(' '))

        yield {
        'reference': response.url.split('/')[6],
        'title': get_css('div.box div.header h2::text'),
        'publication_date': get_xpath('//div[@id="advanced_search"]/li[1]/span[@class="value"]/text()'),
        'country': get_xpath('//div[@id="advanced_search"]/li[2]/span[@class="value"]/text()').split(' ')[loc_arr_len - 1],
        'location_name': get_xpath('//div[@id="advanced_search"]/li[2]/span[@class="value"]/text()').split(' ')[0],
        'postal_code': get_xpath('//div[@id="advanced_search"]/li[2]/span[@class="value"]/text()').split(' ')[loc_arr_len - 2].replace("(", "").replace("),", ""),
        'education_level': get_xpath('//div[@id="advanced_search"]/li[4]/span[@class="value"]/text()'),
        'experience_level': get_xpath('//div[@id="advanced_search"]/li[5]/span[@class="value"]/text()'),
        'contract_type': get_xpath('//div[@id="advanced_search"]/li[3]/span[@class="value"]/text()'),
        'job_description': get_xpath('//ul[@class="content description"]/li[1]/p').replace(";", ","),
        'profile_description': get_xpath('//ul[@class="content description"]/li[2]/p').replace(";", ","),
        }